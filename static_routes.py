#Declare the targeted template and vsys
templateName = ""
vsys = ""

#route names
names = [

]  

#routes next-hop
nxthop = [

]

#routes destination interface
interfaces = [

]

i = 0
#print(len(routes));
#print(len(interfaces));
#print(len(names));
#print(len(nxthop));

for route in routes:
    print("set template ", templateName,"config network virtual-router ", vsys," routing-table ip static-route ", names[i],"nexthop ip-address ", nxthop[i]);
    print("set template ", templateName,"config network virtual-router ", vsys," routing-table ip static-route ", names[i]," bfd profile None");
    print("set template ", templateName,"config network virtual-router ", vsys," routing-table ip static-route ", names[i]," interface ", interfaces[i]);
    print("set template ", templateName,"config network virtual-router ", vsys," routing-table ip static-route ", names[i]," metric 10");
    print("set template ", templateName,"config network virtual-router ", vsys," routing-table ip static-route ", names[i]," destination ", routes[i]);
    print("set template ", templateName,"config network virtual-router ", vsys," routing-table ip static-route ", names[i]," route-table unicast");

